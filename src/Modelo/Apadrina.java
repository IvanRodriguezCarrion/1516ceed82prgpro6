package Modelo;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Apadrina  {
/**
 * Variables de clase (que verán todas las funciones del Apadrina
 y corresponden a los atributos de la tabla APADRINA
 */
  private String idapadrina;
  private Apadrina apadrina;
  private Padrino padrino;
  private Perro perro;
 
 public Apadrina (){ //Constructor de objetos tipo Apadrina genérico
    }
  
 public Apadrina (String id) { //Constructor de objetos tipo Apadrina genérico
       idapadrina = id;
    }
 
 public Apadrina (String id, Padrino pa, Perro pe){
       idapadrina = id;
       padrino = pa;
       perro = pe;
 
 }
 
 public Apadrina (Apadrina ap, Padrino pa, Perro pe){
       apadrina = ap;
       padrino = pa;
       perro = pe;
 }
// 
// /**
//     * Constructor de objetos tipo Apadrina que
//     * contiene las variables que le tenemos que pasar
//     * para definirlo.
//  */ 
// 
// public Apadrina (int numapadrina, int numpadrino, int numperro) { 
//       idapadrina = numapadrina;
//       Padrino idpadrino = numpadrino;
//       idperro = numperro;
//    }
 
 /**
     * @return the idApadrina
     * o método para devolver la idApadrina
     */
 
 public String getIdApadrina(){
   return idapadrina;
 } 
 
 /**
     * @return the idApadrina
     * o método para devolver la idPadrino
     * de la clase Padrino
     */
 
 public Padrino getPadrino(){
   return padrino;
 }
 
  /**
     * @return the idPerro
     * o método para devolver el idPerro
     * de la clase Perro
     */
 
 public Perro getPerro(){
   return perro;
 }
 
  /**
    * @param ida the idapadrina to set
    * o método para modificar la idapadrina
  */
    
    public void setIdApadrina(String ida) {
        idapadrina = ida;
    }
    
  /**
    * @param padrino the padrino to set
    * o método para modificar la infor
  */
    
    public void setPadrino(Padrino padrino) {
        this.padrino = padrino;
    }     
 
  /**
    * @param perro the perro to set
    * o método para modificar la información de Perro 
    
  */
    
    public void setPerro(Perro perro) {
        this.perro = perro;
    } 
    
}