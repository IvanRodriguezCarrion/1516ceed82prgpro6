package Modelo;

import java.util.HashSet;
import java.util.Iterator;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ModeloHashSet implements IModelo {

    
    private HashSet<Padrino> padrinos = new HashSet();
    private HashSet<Perro> perros = new HashSet();
    private HashSet<Apadrina> apadrinas = new HashSet();
    
    int idpadrino = 0;
     int idperro = 0;
      int idapadrina = 0;

    @Override
    public void create(Padrino padrino) {
        padrinos.add(padrino);
        idpadrino++;
    }

    public void create(Perro perro) {
        perros.add(perro);
        idperro++;
    }
    
    public void create(Apadrina apadrina) {
        apadrinas.add(apadrina);
        idapadrina++;
    }
    
    
    public HashSet readPadrino() {
        return padrinos;
    }
    public HashSet readPerro() {
        return perros;
    }
    public HashSet readApadrina() {
        return apadrinas;
    }

    @Override
    public void update(Padrino padrino) {
        Iterator it = padrinos.iterator();
        Padrino pa;

        while (it.hasNext()) {
            pa = (Padrino) it.next();
            if (pa.getIdPersona().equals(padrino.getIdPersona())) {
                it.remove();
               }
            
        } padrinos.add(padrino);
    }
    
    @Override
    public void update(Perro perro) {
        Iterator it = perros.iterator();
        Perro pe;

        while (it.hasNext()) {
            pe = (Perro) it.next();
            if (pe.getIdPerro().equals(perro.getIdPerro())) {
                it.remove();
               }
            
        } perros.add(perro);
    }
    
    @Override
    public void update(Apadrina apadrina) {
        Iterator it = apadrinas.iterator();
        Apadrina apa;

        while (it.hasNext()) {
            apa = (Apadrina) it.next();
            if (apa.getIdApadrina().equals(apadrina.getIdApadrina())) {
                it.remove();
               }
            
        } apadrinas.add(apadrina);
    }

    @Override
    public void delete(Padrino alumno) {
        Iterator it = padrinos.iterator();
        Padrino pa;

        while (it.hasNext()) {
            pa = (Padrino) it.next();
            if (pa.getIdPersona().equals(alumno.getIdPersona())) {
                it.remove();
            }
        }
    }
    
     public void delete(Perro perro) {
        Iterator it = perros.iterator();
        Perro pe;

        while (it.hasNext()) {
            pe = (Perro) it.next();
            if (pe.getIdPerro().equals(perro.getIdPerro())) {
                it.remove();
            }
        }
    }
     

     public void delete(Apadrina apadrina) {
        Apadrina eliminar = null;
        for(Apadrina a: apadrinas) {
            if (a.getIdApadrina().equals(apadrina.getIdApadrina())) {
                eliminar = a;
            }
        }
        apadrinas.remove(eliminar);
    }
    
}
