package Modelo;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Persona {
 
 /**
 * Variables de clase (que verán todas las funciones del ModeloPadrino
 * y corresponden a los atributos de la tabla PERRO
 */
  public String idpersona;
  public String nombre;
//  public String apellidos;
//  public String DNI;
//  public String direccion;
  public String telefono;
 
  
 
  
  
public Persona () { //Constructor de objetos tipo Persona genérico
       
    }

 /**
     * Constructor de objetos tipo Persona que
     * contiene las variables que le tenemos que pasar
     * para definirlo.
  */ 
 
 public Persona (String idp, String name, String telf) { 
       idpersona = idp;
       nombre = name;
       telefono = telf;
       
       
    }
 
 /**
     * @return the idpersona
     * o método para devolver la idpersona
     */
 
 public String getIdPersona(){
   return idpersona;

   }
 
  /**
     * @return the Nombre
     * o método para devolver el Nombre
     */
 
 public String getNombre(){
   return nombre;

   }
 
 /**
     * @return the apellidos
     * o método para devolver los apellidos
     */
 
  
  /**
     * @return the telefono
     * o método para devolver el telefono
     */
 
 public String getTelefono(){
   return telefono;

   }
    
 /**
    * @param idp the idperro to set
    * o método para modificar la idpersona
  */
    
    public void setIdPersona(String idp) {
        idpersona = idp;
    } 

  /**
    * @param name the nombre to set
    * o método para modificar el nombre
  */
    
    public void setNombre(String name) {
        nombre = name;
    } 


  /**
    * @param telf the raza to telefono
    * o método para modificar el telfono
  */
    
    public void setTelefono(String telf) {
        telefono = telf;
    }   
  
   public void setId(String idpersona) {
    this.idpersona = idpersona;
  }

}