package Modelo;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Perro {
  
  /**
 * Variables de clase (que verán todas las funciones del ModeloPadrino
 * y corresponden a los atributos de la tabla PERRO
 */
  public String idperro;
  public String nombre;
  public String nChip;
  public String raza;
  
  
public Perro () { //Constructor de objetos tipo Perro genérico
       
    }

 /**
     * Constructor de objetos tipo Perro que
     * contiene las variables que le tenemos que pasar
     * para definirlo.
  */ 
 
 public Perro (String idp, String name, String chip, String race) { 
       idperro = idp;
       nombre = name;
       nChip = chip;
       raza = race;
       
    }
 
 /**
     * @return the idPerro
     * o método para devolver la idPerro
     */
 
 public String getIdPerro(){
   return idperro;

   }
 
  /**
     * @return the Nombre
     * o método para devolver el Nombre
     */
 
 public String getNombre(){
   return nombre;

   }
 
  /**
     * @return the nChip
     * o método para devolver la nChip
     */
 
 public String getNChip(){
   return nChip;

   }
 
  /**
     * @return the raza
     * o método para devolver la raza
     */
 
 public String getRaza(){
   return raza;

   }
 
    
 /**
    * @param idp the idperro to set
    * o método para modificar la idPerro
  */
    
    public void setIdPerro(String idp) {
        idperro = idp;
    } 

  /**
    * @param name the nombre to set
    * o método para modificar el nombre
  */
    
    public void setNombre(String name) {
        nombre = name;
    } 

  /**
    * @param chip the nChip to set
    * o método para modificar el número de chip
  */
    
    public void setNChip(String chip) {
        nChip = chip;
    }     
    
  /**
    * @param race the raza to set
    * o método para modificar la raza
  */
    
    public void setRaza(String race) {
        raza = race;
    } 
}