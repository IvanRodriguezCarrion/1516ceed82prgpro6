package Modelo;

import java.util.HashSet;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/


public interface IModelo {

      	//Padrino
	public void create(Padrino padrino);
	public HashSet <Padrino> readPadrino();
	public void update(Padrino padrino);
	public void delete(Padrino padrino);
	

	//Perro
	public void create(Perro perro);
	public HashSet <Perro> readPerro();
	public void update(Perro perro);
	public void delete(Perro perro);
	

	//Apadrina
	public void create(Apadrina apadrina);
	public HashSet <Apadrina> readApadrina();
	public void update(Apadrina apadrina);
	public void delete(Apadrina apadrina);
}
