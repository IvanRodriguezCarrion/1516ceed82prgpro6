package Modelo;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Padrino extends Persona {
/**
 * Variables de clase (que verán todas las funciones del Padrino
 y corresponden a los atributos de la tabla PADRINO
 */
  public String email;  
  
public Padrino () { //Constructor de objetos tipo Padrino genérico
       super();
    }

 /**
     * Constructor de objetos tipo Padrino que
     * contiene las variables que le tenemos que pasar
     * para definirlo. Incluye los atributos de PERSONA
     * heredados primero indicando "extends" en la class
     * y luego utilizando "super" en el constructor, que permite
     * indicarle que variables se están heredando (y han de incluirse
     * en los atributos del constructor aunque no estén definidas)
  */ 
 
 public Padrino (String idp, String name, String telf, String mail) { 
       super(idp, name, telf);
       email = mail;
       
       
       
    }
 
 /**
     * @return the email
     * o método para devolver la email
     */
 
 public String getEmail(){
   return email;

   }
  
 /**
    * @param email the mail to set
    * o método para modificar la idPerro
  */
    
    public void setEmail(String mail) {
        email = mail;
    } 
}