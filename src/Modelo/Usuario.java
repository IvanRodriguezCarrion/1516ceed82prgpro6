package Modelo;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Usuario extends Persona {
 /**
 * Variables de clase (que verán todas las funciones del ModeloPadrino
 * y corresponden a los atributos de la tabla USUARIO
 */
  public String password;
  
  
  
public Usuario () { //Constructor de objetos tipo Persona genérico
       super();
    }

 /**
     * Constructor de objetos tipo Usuario que
     * contiene las variables que le tenemos que pasar
     * para definirlo. Incluye los atributos de PERSONA
     * heredados primero indicando "extends" en la class
     * y luego utilizando "super" en el constructor, que permite
     * indicarle que variables se están heredando (y han de incluirse
     * en los atributos del constructor aunque no estén definidas)
  */ 
 
 public Usuario (String idp, String name, String telf, String pass) { 
       super(idp, name, telf);
       password = pass;
              
    }

  /**
     * @return the password
     * o método para devolver el password
     */
 
 public String getPassword(){
   return password;

   }
 
  /**
    * @param pass the password to set
    * o método para modificar el password
  */
    
    public void setPassword(String pass) {
        password = pass;
    }     
    
  
  

}