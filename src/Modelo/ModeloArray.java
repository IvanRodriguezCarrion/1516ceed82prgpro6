package Modelo;

import java.util.HashSet;

/**
 ** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
 */

public class ModeloArray implements IModelo {
    private Padrino padrinos[] = new Padrino[100];
    private Perro perros[] = new Perro[100];
    private Apadrina apadrinas[] = new Apadrina[100];
    private Padrino padrinovacio = new Padrino("","","","");
    private Padrino padrino = new Padrino();
    private Perro perrovacio = new Perro("","","","");
    private Perro perro = new Perro();
    private Apadrina apadrinavacio = new Apadrina("",null, null);
    private Apadrina apadrina = new Apadrina();
    int idPad = 0;
    int idPer = 0;
    int idApa = 0;
   
    
public ModeloArray(){
  
        for (int contador=0;contador<padrinos.length;contador++){
            padrinos[contador]=new Padrino("","","","");
        }
        for (int contador=0;contador<perros.length;contador++){
            perros[contador]=new Perro("","","","");
        }
        for (int contador=0;contador<apadrinas.length;contador++){
            apadrinas[contador]=new Apadrina("",null, null);
        }
}
    
  public void create(Padrino padrino) {
        padrinos[idPad] = padrino;
        idPad++;
    }
  
  public void create(Perro perro) {
        perros[idPer] = perro;
        idPer++;
    }
  
   public void create(Apadrina apadrina) {
        apadrinas[idApa] = apadrina;
        idApa++;
    }
  

  public HashSet readPadrino() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < padrinos.length; i++) {
            if ( !padrinos[i].getIdPersona().equals("")) {
                hashset.add(padrinos[i]);
            }
        }
        return hashset;
    }
  
   public HashSet readPerro() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < perros.length; i++) {
            if ( !perros[i].getIdPerro().equals("")) {
                hashset.add(perros[i]);
            }
        }
        return hashset;
    }
   
    @Override
   public HashSet readApadrina() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < apadrinas.length; i++) {
            if ( !apadrinas[i].getIdApadrina().equals("")) {
                hashset.add(apadrinas[i]);
            }
        }
        return hashset;
    }

    @Override
    public void update(Padrino padrino) {
        int i = 0;
        while (i < padrinos.length) {
            if (padrinos[i].getIdPersona().equals(padrino.getIdPersona())) {
                padrinos[i] = padrino;
            }
        i++;
        }
    }
    
    @Override
    public void update(Perro perro) {
        int i = 0;
        while (i < perros.length) {
            if (perros[i].getIdPerro().equals(perro.getIdPerro())) {
                perros[i] = perro;
            }
        i++;
        }
    }
    
   
     public void update(Apadrina apadrina) {
    int i = 0;
        while (i < apadrinas.length) {
            if (apadrinas[i].getIdApadrina().equals(apadrina.getIdApadrina())) {
                apadrinas[i] = apadrina;
            }
        i++;
        }
        
    }
     
//     public void comprobarIdPadrinoPerro(String idpadrino, String idperro) {
//       Apadrina padrina = null;
//       Padrino padrino = null;
//       Perro perro = null;
//         for (int i = 0; i < padrinos.length || i < perros.length ; i++) {
//            if (padrinos[i].getIdPersona().equals(idpadrino)) {
////                padrino = padrinos[i];
//            } 
//                          
//            if (perros[i].getIdPerro().equals(idperro)) {
////                perro = perros[i];
//            }
//        } 
//    }
     
    
    @Override
    public void delete(Padrino padrino) {
        int i = 0;
        while (i < padrinos.length) {
            if (padrinos[i].getIdPersona().equals(padrino.getIdPersona())) {
                padrinos[i] = padrinovacio;
            }
        i++;
        }
    }
    
    @Override
        public void delete(Perro perro) {
        int i = 0;
        while (i < perros.length) {
            if (perros[i].getIdPerro().equals(perro.getIdPerro())) {
                perros[i] = perrovacio;
            }
        i++;
        }
    }
        
    @Override
        public void delete(Apadrina apadrina) {
        int i = 0;
        while (i < apadrinas.length) {
            if (apadrinas[i].getIdApadrina().equals(apadrina.getIdApadrina())) {
                apadrinas[i] = apadrinavacio;
            }
        i++;
        }
    }
  
}