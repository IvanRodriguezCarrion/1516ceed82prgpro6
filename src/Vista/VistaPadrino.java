package Vista;

import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import Modelo.Padrino;
import java.util.HashSet;
import java.util.Iterator;
/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaPadrino implements IVista <Padrino>{
  
  private String idpersona, nombre, telefono, email;
  
  
      public static char menuCrud() {
        char opt;
        System.out.println("\n******************************");
        System.out.println("------- MENÚ C.R.U.D. --------");
        System.out.println("---- e. Salir (exit) ---------");
        System.out.println("---- c. Crear (create) -------");
        System.out.println("---- r. Leer (read) ----------");
        System.out.println("---- u. Actualizar (update) --");
        System.out.println("---- d. Borrar (delete) ------");
        System.out.println("******************************");
        opt = VistaPrincipal.leerCaracter();
        return opt;
    }
  
  
  public Padrino tomaDatos () throws InputMismatchException {
                                   
  System.out.println("Crear nuevo padrino: \n================");
            
  System.out.println("Introduzca la ID de la persona");
  idpersona = VistaPrincipal.leerTexto();
  
 System.out.println("Introduzca el nombre");
 nombre = VistaPrincipal.leerTexto();

 System.out.println("Introduzca el telefono");
 telefono = VistaPrincipal.leerTexto();

 boolean salir = true;
 
 do{
     System.out.println("Introduzca el email (tuemail@tuemail.com)");
     email = VistaPrincipal.leerTexto();
     boolean correcto = VistaPrincipal.comprobarEmail(email);
                if (correcto) {
                    salir = false;
                } else {
                    System.err.println("El e-mail introducido no es válido.");
                }
} while(salir);
   
  Padrino padrino = new Padrino(idpersona, nombre , telefono, email);
  
  return padrino; 
  }
  
  public void muestraDatos(Padrino p) {
    System.out.println("Mostrando Datos: ");
    System.out.println("Identificador de la persona: "+p.getIdPersona());
    System.out.println("Nombre: "+p.getNombre());
    System.out.println("Email: "+p.getEmail());
    System.out.println("Teléfono: "+p.getTelefono()+"\n=======================");
    

  }
  
  public void muestraPadrinos(HashSet hs) {
    IVista<Padrino> v = new VistaPadrino();
    Iterator it = hs.iterator();
    Padrino padrino = null;
       
    System.out.println("Datos de los Padrinos \n===============");
    while (it.hasNext()) {
      padrino = (Padrino) it.next();
      v.muestraDatos(padrino);
    }
  }
  
  public String actualizarId() {

    System.out.print("Introduzca el identificador único del padrino a actualizar: ");
    String linea = VistaPrincipal.leerTexto();

    return linea;

  }
  
   public String borrarId() {

    System.out.print("Introduzca el identificador único del padrino a borrar: ");
    String linea = VistaPrincipal.leerTexto();

    return linea;

  }
   
   public void errorIdPadrino() {

    System.out.print("\n========================\nError en el ID del padrino. Vuelva a introducirlo\n========================\n");
    

  }
  
   
    
  
}