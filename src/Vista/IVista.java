package Vista;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public interface IVista <T> {
    
    public T tomaDatos();
    public void muestraDatos(T t);
}