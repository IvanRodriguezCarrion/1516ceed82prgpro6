package Vista;

import java.util.Scanner;
import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import java.io.IOException;
import Modelo.Perro;
import java.util.HashSet;
import java.util.Iterator;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaPerro implements IVista <Perro>{
  
  String idperro, nombre, raza, nchip;
  
  public static char menuCrud() {
        char opt;
        System.out.println("\n******************************");
        System.out.println("------- MENÚ C.R.U.D. --------");
        System.out.println("---- e. Salir (exit) ---------");
        System.out.println("---- c. Crear (create) -------");
        System.out.println("---- r. Leer (read) ----------");
        System.out.println("---- u. Actualizar (update) --");
        System.out.println("---- d. Borrar (delete) ------");
        System.out.println("******************************");
        opt = VistaPrincipal.leerCaracter();
        return opt;
    }
  
  public Perro tomaDatos () throws InputMismatchException {
 
  
                        
  System.out.println("Crear nuevo perro: \n================");
  
  System.out.println("Introduzca la ID del perro");
  idperro = VistaPrincipal.leerTexto();
      
  System.out.println("Introduzca su nombre:");
  nombre = VistaPrincipal.leerTexto();
  
//   boolean salir_nombre = false; //Esta es la condición para que salga se salga del bucle while
//  
//  while (!salir_nombre) { //Mientras no-salir, o sea, mientras salir sea falso ha de continuar en el bucle.
//
//      try {
//
//         System.out.println("Introduzca el número de chip");
//         nchip = VistaPrincipal.leerNumero();
//         salir_nombre = true;        
//
//      }
//      catch (InputMismatchException e) { //Hemos escogido capturar esta excepción pues es la relacionada con la clase Scanner
//        VistaPrincipal.error();
//      }
//    }
  boolean salir = true;
  do{
     System.out.println("Introduzca el numero de chip (15 dígitos):");
     nchip = VistaPrincipal.leerTexto();
//     String numero = Integer.toString(nchip);
     boolean correcto = VistaPrincipal.comprobarChip(nchip);
                if (correcto) {
                    salir = false;
                } else {
                    System.err.println("El número de chip introducido no es válido.");
                }
                
} while(salir);
 
  
  System.out.println("Introduzca su raza:");
  raza = VistaPrincipal.leerTexto();
  
  
  
  Perro perro = new Perro(idperro, nombre, nchip, raza);
  
  return perro; 
  }
  
  public void muestraDatos(Perro perro) {
    System.out.println("-------------------------------- ");
    System.out.println("| Mostrando Datos del Perro: ");
    System.out.println("| Identificador del perro: "+perro.getIdPerro());
    System.out.println("| Nombre: "+perro.getNombre());
    System.out.println("| Número de Chip: "+perro.getNChip());
    System.out.println("| Raza: "+perro.getRaza());
    System.out.println("-------------------------------- ");
  }
  
  public void muestraPerros(HashSet hs) {
    IVista<Perro> v = new VistaPerro();
    Iterator it = hs.iterator();
    Perro perro = null;
       
    System.out.println("Datos de los perros \n===============");
    while (it.hasNext()) {
      perro = (Perro) it.next();
      v.muestraDatos(perro);
    }
  }
  
  public String actualizarId() {

    System.out.print("Introduzca el identificador único del perro a actualizar: ");
    String linea = VistaPrincipal.leerTexto();

    return linea;

  }
  
  public String borrarId() {

    System.out.print("Introduzca el identificador único del perro a borrar: ");
    String linea = VistaPrincipal.leerTexto();

    return linea;

  }
  
     public void errorIdPerro() {

    System.out.print("=====================\nError en el ID del perro. Vuelva a introducirlo.\n=====================\n");
     }
}