package Vista;

import java.util.Scanner;
import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import java.io.IOException;
import Modelo.Persona;
/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaPersona implements IVista <Persona>{
  
  String idpersona, nombre, apellidos, dni, direccion, telefono;
  
  public Persona tomaDatos () throws InputMismatchException {
  
  
                               
  System.out.println("Crear nueva Persona: \n================");
  
  System.out.println("Introduzca la ID de la persona");
  idpersona = VistaPrincipal.leerTexto();
  
   System.out.println("Introduzca el nombre");
   nombre = VistaPrincipal.leerTexto();
   
//   System.out.println("Introduzca los apellidos");
//   apellidos = teclado.nextLine();
//   
//   System.out.println("Introduzca el DNI");
//   dni = teclado.nextLine();
//   
//   System.out.println("Introduzca la dirección");
//   direccion = teclado.nextLine();
   
   System.out.println("Introduzca el telefono");
   telefono = VistaPrincipal.leerTexto();
   
   
   
   
   
  Persona persona = new Persona(idpersona, nombre, telefono);
  
  return persona; 
  }
  
  public void muestraDatos(Persona persona) {
    
  }
  
}