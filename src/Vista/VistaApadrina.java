package Vista;

import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import Modelo.Apadrina;
import Modelo.Perro;
import Modelo.Padrino;
import java.util.HashSet;
import java.util.Iterator;
//import Vista.VistaPerro;
//import Vista.VistaPadrino;


/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class VistaApadrina implements IVista <Apadrina> {
  
  private String idapadrina;
  private Padrino padrino;
  private Perro perro;
  
  public static char menuCrud() {
        char opt;
        System.out.println("\n******************************");
        System.out.println("------- MENÚ C.R.U.D. --------");
        System.out.println("---- e. Salir (exit) ---------");
        System.out.println("---- c. Crear (create) -------");
        System.out.println("---- r. Leer (read) ----------");
        System.out.println("---- u. Actualizar (update) --");
        System.out.println("---- d. Borrar (delete) ------");
        System.out.println("******************************");
        opt = VistaPrincipal.leerCaracter();
        return opt;
    }
  
  
  public Apadrina tomaDatos ()  throws InputMismatchException {
   
  System.out.println("Crear nuevo apadrinamiento: \n================");
 
  System.out.println("Introduzca la ID del apadrinamiento");
  idapadrina = VistaPrincipal.leerTexto();
  
        
    Apadrina apadrina = new Apadrina (idapadrina, padrino, perro);
    
    return apadrina; 
  }
  
  public void muestraDatos(Apadrina apadrina) {
    
    
    VistaPerro vperro = new VistaPerro();
    VistaPadrino vpadrino = new VistaPadrino();

    System.out.println("Datos del apadrinamiento: ");
    System.out.println("Identificador del apadrinamiento: "+apadrina.getIdApadrina());
    System.out.println("=======================");
    vpadrino.muestraDatos(apadrina.getPadrino());
    
    System.out.println("=======================");
    vperro.muestraDatos(apadrina.getPerro());
  }
  
  public void mostrarApadrinamientos(HashSet hashset) {
        IVista<Apadrina> vistaapadrina = new VistaApadrina();
        Iterator iterator = hashset.iterator();
        Apadrina apadrina = null;

        while (iterator.hasNext()) {
            apadrina = (Apadrina) iterator.next();
            vistaapadrina.muestraDatos(apadrina);
            System.out.println("--------------------------------");
        }
    }
  
  
  
  public String pedirIdPadrino() {

    System.out.print("Introduzca el identificador único del padrino: ");
    String linea = VistaPrincipal.leerTexto();

    return linea;

  }
  
  public String pedirIdPerro() {

    System.out.print("Introduzca el identificador único del perro: ");
    String linea = VistaPrincipal.leerTexto();

    return linea;

  }
  
  public String actualizarId() {

    System.out.print("Introduzca el identificador único del apadrinamiento a actualizar: ");
    String linea = VistaPrincipal.leerTexto();

    return linea;

  }
  
   public String borrarId() {

    System.out.print("Introduzca el identificador único del apadrinamiento a borrar: ");
    String linea = VistaPrincipal.leerTexto();

    return linea;

  }
    
   public void datosIntroducidos(){
     System.out.print("Datos introducidos correctos.");
   }
   
    public void errorIdApadrina() {

    System.out.print("Error en el ID del apadrinamiento");
    }
}
