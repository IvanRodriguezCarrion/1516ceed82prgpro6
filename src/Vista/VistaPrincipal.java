package Vista;

import java.util.Scanner;
import java.util.InputMismatchException; //·Importamos esta excepción para el trycatch
import Controlador.Configuracion;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class VistaPrincipal {
 
  
public static int MuestraMenu () throws InputMismatchException {
  
  int opcion = 5;

  String titulo = Configuracion.getTitulo();
    

    System.out.println(titulo);
    System.out.println("\nMENÚ APADRINAMIENTO\n================");
    System.out.println("Escoja una de las siguientes opciones: ");
    System.out.println("0.- Sección de Padrinos:");
    System.out.println("1.- Sección de Perros:");
    System.out.println("2.- Sección de Apadrinamientos:");
    System.out.println("3.- Salir. \n================");
    opcion = leerNumero();
    return opcion;
      }

public char menuEstructura(){

    char opcion = ' ';

    System.out.println("===== MENU ESTRUCTURA ===== ");
    System.out.println("|---------e. exit---------| ");
    System.out.println("|--a. Operar vía Arrays---| ");
    System.out.println("|--s. Operar vía HashSet--| ");
    System.out.println("|--f. Operar vía Ficheros-| ");
    System.out.println("===========================");
    System.out.print("Opción:  ");
    opcion = leerCaracter();

    return opcion;

  }

  public static void error() {
        System.err.println("El valor introducido no es numérico. Por favor, introduce un número.");
    }

public static String leerTexto() {
        Scanner scanner = new Scanner(System.in); 
        String texto = scanner.nextLine();
        return texto;
    }
    
    public static int leerNumero() {
        Scanner scanner = new Scanner(System.in); 
        int numero = scanner.nextInt();
        return numero;
    }

   public static char leerCaracter() {
        Scanner scanner = new Scanner(System.in);
        String cadena = scanner.next().toLowerCase();
        char caracter = cadena.charAt(0);
        return caracter;
    }
    
    public static void salida() {
        System.out.println("Saliendo del menú.");
    }
    
    public static void errorMenuCrud() {
        System.err.println("\nEscoge alguna de las opciones del menú.");
    }
    
    public static String leerTextoMinuscula() {
        Scanner scanner = new Scanner(System.in); 
        String texto = scanner.nextLine();
        texto = scanner.next().toLowerCase();
        return texto;
    }    

    public static void borradoCorrectoPadrino() {

    System.out.print("\n========================\nSe han borrado correctamente tanto el Padrino como el Apadrinamiento asociado \n========================\n");
    

  }
    
    public static void noBorrado() {

    System.out.print("\n========================\nNo se han borrado los datos \n========================\n");
    

  }
    
    public static char menuBorrado(){

    

    System.out.println("El padrino o perro seleccionados"
            + "para eliminar está dado de alta en un apadrinamiento."
            + "Su eliminación conllevará la desaparición del apadrinamiento también, ¿desea continuar?");
    System.out.print("Opción(s/n):");
    char opcion = leerCaracter();

    return opcion;

  }
    
    public static void insertarTextoNormal(String texto) {
        System.out.print(texto);
    }
    
    public static void insertarTextoError(String texto) {
        System.err.print(texto);
    }
    
    public static boolean comprobarEmail(String s) {
        Pattern pattern = Pattern.compile("[\\w\\.]+@\\w+\\.\\w+");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
    
    public static boolean comprobarChip(String i) {
        Pattern pattern = Pattern.compile("\\d{15}");
        Matcher matcher = pattern.matcher(i);
        return matcher.matches();
    }
    
    }
    
    
    
  
  
  
  
 

