package Controlador;
import Controlador.ControladorPrincipal;
import Modelo.Padrino;
import Modelo.Perro;
import Modelo.Apadrina;
import Modelo.IModelo;
import Vista.IVista;
import Vista.VistaPadrino;
import Vista.VistaPerro;
import Vista.VistaApadrina;
import Vista.VistaPrincipal;
import java.util.InputMismatchException;



/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class Main {
  // Instanciamos objetos del Modelo
        private static Padrino padrino = new Padrino();
        private static  Perro perro = new Perro();
        private static  Apadrina apadrina = new Apadrina();
        
        //Instanciamos los objetos de la Vista, vía interface.
        private static  IVista<Padrino> vistapadrino = new VistaPadrino();
        private static  IVista<Perro> vistaperro = new VistaPerro();
        private static  IVista<Apadrina> vistaapadrina = new VistaApadrina();
        
        
  public static void main (String[] args) throws InputMismatchException {
   
   /**
    * La clase Main se encargará de controlar la ejecución
    * de los procesos del programa importando las clases
    * de los paquetes modelo y vista.
   */ 
      
    Configuracion.getInstance("PROGRAMA DE APADRINAMIENTO"); //Instanciamos por primera vez. 
    
        IModelo modelo = null;
	VistaPrincipal vista = new VistaPrincipal ();
	ControladorPrincipal cp = new ControladorPrincipal (modelo, vista);

    }
}  