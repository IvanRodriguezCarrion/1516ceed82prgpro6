package Controlador;
import Vista.VistaPrincipal;
import Modelo.ModeloArray;
import Modelo.ModeloHashSet;
import Modelo.ModeloFichero;
import Modelo.IModelo;
import java.util.InputMismatchException;



/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorPrincipal {
    IModelo imodelo;
    VistaPrincipal vistaprincipal = new VistaPrincipal();
    
  public ControladorPrincipal (IModelo m, VistaPrincipal v) throws InputMismatchException {
    vistaprincipal = v;
    imodelo = m;
    boolean ciclo = true;
   
    do{
      try {
        char opcion = vistaprincipal.menuEstructura();
        switch (opcion) {

            case 'e': // Exit
              return;
            case 'a':
              imodelo = new ModeloArray();
              ciclo = false;
              break;
            case 's':
              imodelo = new ModeloHashSet();
              ciclo = false;
              break;
            case 'f':
               imodelo = new ModeloFichero();
               ciclo = false;
               break;}
      }
      catch(InputMismatchException ex){
              VistaPrincipal.error();
              }
   } while(ciclo);
    
   ciclo = true;
   
    do {
            try {
                int opt = VistaPrincipal.MuestraMenu();
                switch (opt) {
                    case 0:
                        ControladorPadrino cpadrino = new ControladorPadrino(imodelo);
                        break;
                    case 1:
                        ControladorPerro cperro = new ControladorPerro(imodelo);
                        break;
                    case 2:
                        ControladorApadrina capadrina = new ControladorApadrina(imodelo);
                        break;
                    case 3:
                        VistaPrincipal.salida();
                        ciclo = false;
                        break;
                }
            } catch (InputMismatchException e) {
                VistaPrincipal.error();
            }
        } while (ciclo);
 }

  
    
    
    
    
}
