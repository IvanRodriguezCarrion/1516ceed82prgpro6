package Controlador;

import Modelo.IModelo;
import Modelo.Perro;
import Modelo.Apadrina;
import Vista.IVista;
import Vista.VistaPerro;
import Vista.VistaPrincipal;
import java.util.HashSet;
import java.util.Iterator;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorPerro {
  private IModelo imodelo;
  private VistaPerro vistaperro = new VistaPerro();
  private IVista<Perro> ivistaperro = new VistaPerro();
  private Perro perro = new Perro();
  private String id;
  
  public ControladorPerro(IModelo m) {
        
        imodelo = m;
        
        Boolean ciclo = true;
        
        do{
          
            char opcion = VistaPerro.menuCrud();
            Perro perro; 
            
          switch (opcion) {
            case 'e':
                VistaPrincipal.salida();
                return;
            case 'c':
                create();
                break;
            case 'r':
                read();
                break;
            case 'u':
                update();
                break;
            case 'd':
                delete();
                break;
            default:
                VistaPrincipal.errorMenuCrud();
             }
          } 
        while(ciclo);
  }
  
private void create(){
        VistaPrincipal.insertarTextoNormal("\nIntroduzca el identificador del perro "
                + "para comprobar que no existe en la base de datos:\n");
        String id = VistaPrincipal.leerTexto();
        Perro perro = null;
        HashSet perros = imodelo.readPerro();
        Iterator iterator = perros.iterator();
        while (iterator.hasNext()) {
            Perro p = (Perro) iterator.next();
            if (id.equals(p.getIdPerro())) {
                VistaPrincipal.insertarTextoNormal("\n========================================================="
                        + "\nEl identificador único ya existe, por favor, escoja otro."
                        + "\n=========================================================");
                return;
            }
        }
        perro = ivistaperro.tomaDatos();
        perro.setIdPerro(id);
        imodelo.create(perro);
     }
    
  
  private void read(){
    HashSet hs = new HashSet();
    hs = imodelo.readPerro();
    vistaperro.muestraPerros(hs);
  }
  
    
  private void delete (){
      String id = vistaperro.borrarId();
      
      Perro perro_seleccionado = null;
      Apadrina apadrinamiento = null;
      
      HashSet perros = imodelo.readPerro();
      Iterator it_perro = perros.iterator();
      
      while (it_perro.hasNext()) {//Con este bucle buscamos si el perro introducido existe.
            perro = (Perro) it_perro.next();
            if (id.equals(perro.getIdPerro())) {
              perro_seleccionado = perro;
              imodelo.delete(perro_seleccionado);
              VistaPrincipal.insertarTextoNormal("\n Perro borrado con éxito. Volviendo al menú.\n");
                  break;
               } 
             
      
//      if (perro_seleccionado !=null){
//      HashSet apadrinamientos = imodelo.readApadrina();
//      Iterator it_apadrina = apadrinamientos.iterator();
//      while (it_apadrina.hasNext()) { //Con este controlamos que exista dentro de un apadrinamiento.
//            Apadrina apadrina = (Apadrina) it_apadrina.next();
//
//        if (perro_seleccionado.equals(apadrina.getPerro())) {
//            apadrinamiento = apadrina;
//            char eleccion = VistaPrincipal.menuBorrado();
//
//            switch (eleccion) {
//                case 's':
//                    imodelo.delete(apadrinamiento);
//                    imodelo.delete(perro_seleccionado);
//                    VistaPrincipal.borradoCorrectoPadrino();
//                    return;
//                case 'n':
//                    VistaPrincipal.insertarTextoNormal("\n============================\n"
//                            + "Cancelada la actualización. Saliendo del actualizador."
//                            + "\n============================\n");
//                    return;
//
//                default:
//                    VistaPrincipal.errorMenuCrud();
//                    return;
//         } } else {imodelo.delete(perro_seleccionado);
//                        VistaPrincipal.insertarTextoNormal("\n======================================\n"
//                    + "Borrado padrino con éxito. Saliendo al menú"
//                    + "\n======================================\n");} 
//            
//                }
             
            else {VistaPrincipal.insertarTextoError("\nNo se ha hallado ningún perro con este identificador"
                    + "Saliendo del actualizador.\n");
                 return;}
            }
            
//            imodelo.delete(perro_seleccionado);
//                        VistaPrincipal.insertarTextoNormal("\n======================================\n"
//                    + "Borrado padrino con éxito. Saliendo al menú"
//                    + "\n======================================\n");
  }            
      
      private void update() {
        VistaPrincipal.insertarTextoNormal("\nIntroduzca el código del perro a actualizar:");
        String id = VistaPrincipal.leerTexto();
        Perro perro = null;
        HashSet perros = imodelo.readPerro();
        Iterator iterator = perros.iterator();
        while (iterator.hasNext()) {
            Perro p = (Perro) iterator.next();
            if (id.equals(p.getIdPerro())) {
                perro = p;
            }
        }
        if (perro != null) {
            HashSet apadrinas = imodelo.readApadrina();
            iterator = apadrinas.iterator();
            while (iterator.hasNext()) {
                Apadrina a = (Apadrina) iterator.next();
                if (perro.equals(a.getPerro())) {
                    VistaPrincipal.insertarTextoNormal("\nEl perro que quieres actualizar tiene un apadrinamieto asociado. "
                            + "Hay que borrar primero el apadrinamiento, actualizar el perro y luego volver a crearla.");
                    return;
                }
            }
            IVista<Perro> vistaperro = new VistaPerro();
            perro = vistaperro.tomaDatos();
            perro.setIdPerro(id);
            imodelo.update(perro);
            VistaPrincipal.insertarTextoNormal("\nPerro actualizado con éxito");
        } else {
            VistaPrincipal.insertarTextoError("\nEl identificador introducido no corresponde"
                    + "a ningún perro.");
        }
      }
}
