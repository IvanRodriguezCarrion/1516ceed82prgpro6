package Controlador;

import java.util.HashSet;
import java.util.Iterator;
import Vista.IVista;
import Vista.VistaApadrina;
import Vista.VistaApadrina;
import Vista.VistaPadrino;
import Vista.VistaPerro;
import Vista.VistaPrincipal;
import Modelo.Padrino;
import Modelo.Perro;
import Modelo.Apadrina;
import Modelo.IModelo;
import Modelo.ModeloArray;




/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorApadrina {
    private IModelo modelo = null;
    
    private IVista<Apadrina> ivistaapadrina = new VistaApadrina();
    private IVista<Padrino> ivistapadrino = new VistaPadrino();
    private IVista<Perro> ivistaperro = new VistaPerro();
    private VistaPadrino vistapadrino = new VistaPadrino();
    private VistaPerro vistaperro = new VistaPerro();
    private VistaApadrina vistaapadrina = new VistaApadrina();
    
//    private ModeloArray modeloarray = new ModeloArray();
//    private Padrino padrino = new Padrino();
//    private Perro perro = new Perro();
//    private Apadrina apadrina = new Apadrina();
//    private String idPad, idPer, idApa;
    
    
    
    
    public ControladorApadrina(IModelo m) {
        modelo = m;
        Boolean ciclo=true;
        
        do {
            char opcion = VistaApadrina.menuCrud();
            Apadrina apadrina;

            switch (opcion) {
                case 'e':
                    VistaPrincipal.salida();
                    return;
                case 'c':
//                  comprobarIdsCreate();
                  create();
                    break;
                case 'r':
                    read();
                    break;
                case 'u':
                    update();
                    break;
                case 'd':
                    delete();
                    break;
                default:
                    VistaPrincipal.errorMenuCrud();
            }
        } while (ciclo);
    }
    
    public void create(){
        
        HashSet padrinos = modelo.readPadrino();
        HashSet perros = modelo.readPerro();
        boolean bpadrino = true;
        boolean bperro = true;
        Apadrina apadrina = new Apadrina("");
        Padrino padrino = new Padrino ("", "", "","");
        Perro perro = new Perro ("","","","");
        
        Padrino padrino_seleccionado = new Padrino();
        Perro perro_seleccionado = new Perro();
                   
        
        VistaPrincipal.insertarTextoNormal("\nIntroduzca el identificador del apadrinamiento "
                + "para comprobar que no existe en la base de datos:\n");
        String id = VistaPrincipal.leerTexto();
        Apadrina apadrina_elegido = null;
        HashSet apadrinas = modelo.readApadrina();
        Iterator iterator = apadrinas.iterator();
        while (iterator.hasNext()) {
            Apadrina apa = (Apadrina) iterator.next();
            if (id.equals(apa.getIdApadrina())) {
                VistaPrincipal.insertarTextoNormal("\n========================================================="
                        + "\nEl identificador único ya existe, por favor, escoja otro."
                        + "\n=========================================================");
                return;
            }
        }
        
        
        apadrina = ivistaapadrina.tomaDatos();
        apadrina.setIdApadrina(id);
        
        
        
        
        do {
        Iterator it_padrino = padrinos.iterator();
        String idPad = "";

        idPad = vistaapadrina.pedirIdPadrino();
        while (it_padrino.hasNext()) {
            padrino = (Padrino) it_padrino.next();
            if (idPad.equals(padrino.getIdPersona())) {
              padrino_seleccionado = padrino;
              bpadrino = false;
              
              }
            }
            
          if (bpadrino) {
          vistapadrino.errorIdPadrino();
          }
        }
        while (bpadrino);
        
        do {
        Iterator it_perro = perros.iterator();
        String idPer = "";
//        perro = new Perro ("","",0,"");
        idPer = vistaapadrina.pedirIdPerro();
        while (it_perro.hasNext()) {
            perro = (Perro) it_perro.next();
            if (idPer.equals(perro.getIdPerro())) {
              perro_seleccionado = perro;
              bperro = false;                
              }
            }
            
          if (bperro) {
          vistaperro.errorIdPerro();
          }
        }
        while (bperro);
//       
      Apadrina apadrinamiento = new Apadrina (apadrina.getIdApadrina(), padrino_seleccionado, perro_seleccionado);
      modelo.create(apadrinamiento);
    }
    
    
    public void read(){
       HashSet hashset = new HashSet();
        hashset = modelo.readApadrina();
        vistaapadrina.mostrarApadrinamientos(hashset);
    }
    
    public void update(){
      //Mediante el read creamos un hashset lleno con los vectores de las tres clases.
        HashSet padrinos = modelo.readPadrino();
        HashSet perros = modelo.readPerro();
        HashSet apadrinamientos = modelo.readApadrina();
        
        boolean bpadrino = true;
        boolean bperro = true;
        boolean bapadrina = true;
        
        Padrino padrino_seleccionado = new Padrino();
        Perro perro_seleccionado = new Perro();
        Apadrina apadrinamiento = new Apadrina();
        
         do{
        
          Iterator it_apadrina = apadrinamientos.iterator();
          String idApa = "";
          Apadrina apadrina = new Apadrina();
          idApa = vistaapadrina.actualizarId();
        
        while (it_apadrina.hasNext()) {
            apadrina = (Apadrina) it_apadrina.next();
            if (idApa.equals(apadrina.getIdApadrina())) {
              apadrinamiento = apadrina; 
              bapadrina = false;                
                } 
//            i++;
            }        
            if (!bpadrino) {
            vistaapadrina.errorIdApadrina();
            }
        } while (bapadrina);
        
        
        do {
        Iterator it_padrino = padrinos.iterator();
        String idPad = "";
        Padrino padrino = new Padrino ("","","","");
        
        idPad = vistaapadrina.pedirIdPadrino();
        while (it_padrino.hasNext()) {
            padrino = (Padrino) it_padrino.next();
            if (idPad.equals(padrino.getIdPersona())) {
              padrino_seleccionado = padrino;
              bpadrino = false;
              
              }
//            i++;
            }
        
            
          if (bpadrino) {
          vistapadrino.errorIdPadrino();
          }
        }
        while (bpadrino);
        
//        i = 0;
        
        do{
        Iterator it_perro = perros.iterator();
        String idPer = "";
        Perro perro = new Perro ("","","","");
        idPer = vistaapadrina.pedirIdPerro();
        while (it_perro.hasNext()) {
            perro = (Perro) it_perro.next();
            if (idPer.equals(perro.getIdPerro())) {
              perro_seleccionado = perro;
              bperro = false;                
                }
//            i++;
            }
        if (bperro) {
          vistaperro.errorIdPerro();
          }
        } while(bperro);
            
        
      vistaapadrina.datosIntroducidos();

      Apadrina apadrina = new Apadrina (apadrinamiento.getIdApadrina(), padrino_seleccionado, perro_seleccionado);
      modelo.update(apadrina);
    }
    
    public void delete(){
      String idApa = vistaapadrina.borrarId();
      HashSet apadrinamientos = modelo.readApadrina();
      Iterator it_apadrina = apadrinamientos.iterator();
      while (it_apadrina.hasNext()) {
            Apadrina apadrina = (Apadrina) it_apadrina.next();
            if (idApa.equals(apadrina.getIdApadrina())) {
              Apadrina apadrinamiento = apadrina;
              Apadrina apadrina_borrado = new Apadrina(idApa, null, null);
              modelo.delete(apadrina);
              VistaPrincipal.insertarTextoNormal("\n========================================================="
                        + "\nApadrinamiento borrado con éxto."
                        + "\n========================================================="); 
              break;
                } 
            }
      VistaPrincipal.insertarTextoError("\nNo se ha hallado la id del apadrinamiento, vuelva a intentarlo\n");
    } 

        
    
    
}
