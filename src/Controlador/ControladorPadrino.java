package Controlador;

import Modelo.Apadrina;
import Vista.VistaPadrino;
import Vista.VistaApadrina;
import Modelo.Padrino;
import Modelo.IModelo;
import Modelo.ModeloFichero;
import java.util.HashSet;
import java.util.Iterator;
import Vista.IVista;
import Vista.VistaPrincipal;
import java.util.Iterator;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
*/

public class ControladorPadrino {
  
  private IModelo imodelo;
  private VistaPadrino vistapadrino = new VistaPadrino();
  private IVista<Padrino> ivistapadrino = new VistaPadrino();
  private Padrino padrino = new Padrino();
//  private String id;
  
  public ControladorPadrino(IModelo m) {
        
        imodelo = m;
        
        Boolean salir = false;
        
        do{
          
            char opcion = VistaPadrino.menuCrud();
            
            
          switch (opcion) {
            case 'e':
                VistaPrincipal.salida();
                return;
            case 'c':
                create();
                break;
            case 'r':
                read();
                break;
            case 'u':
                update();
                break;
            case 'd':
                delete();
                break;
            default:
                VistaPrincipal.errorMenuCrud();
             }
          } 
        while(!salir);
  }
  
  private void create(){
        VistaPrincipal.insertarTextoNormal("\nIntroduzca el identificador del padrino "
                + "para comprobar que no existe en la base de datos:\n");
        String id = VistaPrincipal.leerTexto();
        
        Padrino padrino = null;
        HashSet padrinos = imodelo.readPadrino();
        Iterator iterator = padrinos.iterator();
        while (iterator.hasNext()) {
            Padrino p = (Padrino) iterator.next();
            if (id.equals(p.getIdPersona())) {
                VistaPrincipal.insertarTextoNormal("\n========================================================="
                        + "\nEl identificador único ya existe, por favor, escoja otro."
                        + "\n=========================================================");
                return;
            }
        }
        padrino = ivistapadrino.tomaDatos();
        padrino.setId(id);
        imodelo.create(padrino);
     }
    
  
  
  private void read(){
    HashSet hs = new HashSet();
    hs = imodelo.readPadrino();
    vistapadrino.muestraPadrinos(hs);
  }
  
  private void aupdate(){
     String id = vistapadrino.actualizarId();
      padrino = vistapadrino.tomaDatos();
      padrino.setIdPersona(id);
      imodelo.update(padrino);
  }
  
  
  private void delete (){
      String id = vistapadrino.borrarId();
      Padrino padrino_seleccionado = null;
      Apadrina apadrinamiento = null;
      
      HashSet padrinos = imodelo.readPadrino();
      Iterator it_padrino = padrinos.iterator();
      
      while (it_padrino.hasNext()) {//Con este bucle buscamos si el padrino introducido existe.
            padrino = (Padrino) it_padrino.next();
            if (id.equals(padrino.getIdPersona())) {
              padrino_seleccionado = padrino;
              imodelo.delete(padrino_seleccionado);
              VistaPrincipal.insertarTextoNormal("\n Padrino borrado con éxito. Volviendo al menú.\n");
                  break;
               } 
             
      
//      if (padrino_seleccionado !=null){
//      HashSet apadrinamientos = imodelo.readApadrina();
//      Iterator it_apadrina = apadrinamientos.iterator();
//      while (it_apadrina.hasNext()) { //Con este controlamos que exista dentro de un apadrinamiento.
//            Apadrina apadrina = (Apadrina) it_apadrina.next();
//
//        if (padrino_seleccionado.equals(apadrina.getPadrino())) {
//            apadrinamiento = apadrina;
//            char eleccion = VistaPrincipal.menuBorrado();
//
//            switch (eleccion) {
//                case 's':
//                    imodelo.delete(apadrinamiento);
//                    imodelo.delete(padrino_seleccionado);
//                    VistaPrincipal.borradoCorrectoPadrino();
//                    break;
//                case 'n':
//                    VistaPrincipal.insertarTextoNormal("\n============================\n"
//                            + "Cancelada la actualización. Saliendo del actualizador."
//                            + "\n============================\n");
//                    return;
//
//                default:
//                    VistaPrincipal.errorMenuCrud();
//                    return;
//         } } else {
//            imodelo.delete(padrino_seleccionado);
//            VistaPrincipal.insertarTextoNormal("\n======================================\n"
//                    + "Borrado padrino con éxito. Saliendo al menú"
//                    + "\n======================================\n");
//            }
//            
//                                
//                } //aqui
//            } 
            else {VistaPrincipal.insertarTextoError("No se ha hallado ningún padrino con este identificador"
                    + "Saliendo del actualizador.");
                 return;}
      
      }      
  }            
      
      private void update() {
        VistaPrincipal.insertarTextoNormal("\nIntroduzca el código del padrino a actualizar:");
        String id = VistaPrincipal.leerTexto();
        Padrino padrino = null;
        HashSet padrinos = imodelo.readPadrino();
        Iterator iterator = padrinos.iterator();
        while (iterator.hasNext()) {
            Padrino p = (Padrino) iterator.next();
            if (id.equals(p.getIdPersona())) {
                padrino = p;
//                imodelo.update(padrino); //Actualizador
            }
        }
        if (padrino != null) {
            HashSet apadrinas = imodelo.readApadrina();
            iterator = apadrinas.iterator();
            while (iterator.hasNext()) {
                Apadrina a = (Apadrina) iterator.next();
                if (padrino.equals(a.getPadrino())) {
                    VistaPrincipal.insertarTextoNormal("\nEl padrino que quieres actualizar tiene un apadrinamieto asociado. "
                            + "Hay que borrar primero el apadrinamiento, actualizar el padrino y luego volver a crearla.");
                    return;
                }
            }
            IVista<Padrino> vistapadrino = new VistaPadrino();
            padrino = vistapadrino.tomaDatos();
            padrino.setId(id);
            imodelo.update(padrino);
            VistaPrincipal.insertarTextoNormal("n\nPadrino actualizado con éxito");
        } 
        else {
            VistaPrincipal.insertarTextoError("\nEl identificador introducido no corresponde"
                    + "a ningún padrino.\n");
        }
    }
      
 }
  
  

