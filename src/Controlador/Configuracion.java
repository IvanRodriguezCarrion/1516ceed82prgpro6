package Controlador;

/**
** @author Iván Rodriguez Carrión <ivan.rodriguez.carrion@gmail.com>
* http://migranitodejava.blogspot.com.es/2011/05/singleton.html
*/

public class Configuracion  {
  
  private static Configuracion instance;
  private static String tituloAplicacion;
   //Variable con el nombre del titulo.
  
   /**
   * Se privatiza el constructor para que nadie pueda crear uno (new Configuracion())
   * fuera de esa clase.
   */        
  private Configuracion (String nombre) {
    tituloAplicacion = nombre;
  }
  
  public static Configuracion getInstance(String nombre){
    if (instance == null) {
       instance = new Configuracion(nombre);
      System.out.println("\n**************\nMOSTRANDO APLICACIÓN\n**************");
      }
    else {
            System.err.println("Error, sólo se permite una única instancia de la aplicación.");
          }
    
    return instance;
    }
  
  public static String getTitulo () {
    return tituloAplicacion;
  }
  
    
}